package main

import (
	"embed"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/jmcfarlane/go-quickstart/pkg/metrics"
	"gitlab.com/jmcfarlane/go-quickstart/pkg/templates"
)

var (
	//go:embed static
	static embed.FS

	templatizer *templates.Templatizer = templates.MustNew(static)
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	templatizer.Render(w, r, "static/index.html", struct {
		Title string
	}{
		Title: "test",
	})
}
func main() {
	mc := metrics.New().Register()
	http.Handle("/", mc.ObserveHandlerFunc(indexHandler))
	http.Handle("/foo", mc.WithHandler("foo").ObserveHandlerFunc(indexHandler))
	http.Handle("/bar", mc.ObserveHandlerFunc(indexHandler))
	http.Handle("/bar2", mc.WithHandler("bar2").ObserveHandlerFunc(indexHandler))
	http.Handle("/metrics", promhttp.Handler())
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
