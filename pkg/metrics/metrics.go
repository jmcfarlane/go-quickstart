package metrics

import (
	"net/http"
	"reflect"
	"runtime"
	"time"

	"github.com/jmcfarlane/httprwd"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var reqDuration *prometheus.HistogramVec

// New Config with reqDuration registration
func New(namespaces ...string) *Config {
	var namespace string
	if len(namespaces) > 0 {
		namespace = namespaces[0]
	}
	if namespace == "" {
		namespace = "go_quickstart"
	}
	return &Config{Namespace: namespace}
}

// LogFunc to externalize logging
type LogFunc func(handler string, r *http.Request, code int, dur time.Duration)

// Config for metrics
type Config struct {
	Handler   string
	Namespace string
	LogFunc   LogFunc
}

// Register the basic reqDuration
func (c *Config) Register(buckets ...float64) *Config {
	if len(buckets) == 0 {
		buckets = prometheus.LinearBuckets(0, 5, 20)
	}
	reqDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: c.Namespace,
		Name:      "http_request_duration_seconds",
		Help:      "Histogram of response time for handler",
		Buckets:   buckets,
	}, []string{"code", "method", "handler"})
	return c
}

// WithHandler for use with ObserveHandlerFunc
func (c *Config) WithHandler(h string) *Config {
	config := *c
	config.Handler = h
	return &config
}

// WithLogger for use with logging
func (c *Config) WithLogFunc(f LogFunc) *Config {
	config := *c
	config.LogFunc = f
	return &config
}

// ObserveHandlerFunc metric middleware for http requests
func (c *Config) ObserveHandlerFunc(next http.HandlerFunc) http.Handler {
	handler := handlerName(next, c.Handler)
	obs := reqDuration.MustCurryWith(prometheus.Labels{"handler": handler})
	withMetrics := promhttp.InstrumentHandlerDuration(obs, http.HandlerFunc(next))
	if c.LogFunc == nil {
		return withMetrics
	}
	return c.LoggingHandlerFunc(handler, withMetrics)
}

func (c *Config) LoggingHandlerFunc(handler string, next http.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		d := &httprwd.ResponseWriterDelegate{ResponseWriter: w}
		next.ServeHTTP(d, r)
		c.LogFunc(handler, r, d.Code, time.Since(now))
	})
}

func handlerName(h http.HandlerFunc, handler string) string {
	if handler == "" {
		return runtime.FuncForPC(reflect.ValueOf(h).Pointer()).Name()
	}
	return handler
}
