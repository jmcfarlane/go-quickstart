package templates

import (
	"bytes"
	"embed"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"net/http"

	"github.com/rs/zerolog/log"
)

type (
	// Templatizer for shared use
	Templatizer struct {
		Bytes  bytesFunc
		Render renderFunc
		Write  writeFunc
	}

	// Dict to ease passing arbitrary structures to a template
	Dict struct {
		Key   string
		Value interface{}
	}

	bytesFunc  func(string, interface{}) ([]byte, error)
	renderFunc func(http.ResponseWriter, *http.Request, string, interface{}) error
	writeFunc  func(io.Writer, string, interface{}) error
)

// New templatizer
func New(embedFs embed.FS, funcMaps ...template.FuncMap) (*Templatizer, error) {
	tmpl, err := walk(embedFs, funcMaps...)
	if err != nil {
		return nil, err
	}
	return &Templatizer{
		Bytes:  toBytes(tmpl),
		Render: render(tmpl),
		Write:  write(tmpl),
	}, nil
}

// MustNew returns a templatizer or panics
func MustNew(embedFs embed.FS, funcMaps ...template.FuncMap) *Templatizer {
	tmpl, err := New(embedFs, funcMaps...)
	if err != nil {
		panic(err)
	}
	return tmpl
}

func render(tmpl *template.Template) renderFunc {
	return func(w http.ResponseWriter, r *http.Request, path string, p interface{}) error {
		// TODO: Consider how to extract the logging into the caller
		sl := log.With().Str("path", r.URL.Path).Logger()
		b, err := toBytes(tmpl)(path, p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			sl.Err(err).Msg("Template execution failed")
			return err
		}
		fmt.Fprintln(w, string(b))
		return nil
	}
}

func toBytes(tmpl *template.Template) bytesFunc {
	return func(path string, p interface{}) ([]byte, error) {
		var buf bytes.Buffer
		err := write(tmpl)(&buf, path, p)
		return buf.Bytes(), err
	}
}

func write(tmpl *template.Template) writeFunc {
	return func(wr io.Writer, path string, p interface{}) error {
		return tmpl.ExecuteTemplate(wr, path, p)
	}
}

func mergeFuncMaps(funcMaps ...template.FuncMap) template.FuncMap {
	fm := FuncMap
	for _, m := range funcMaps {
		for k, v := range m {
			fm[k] = v
		}
	}
	return fm
}

func walk(embedFs embed.FS, funcMaps ...template.FuncMap) (*template.Template, error) {
	tmpl := template.New("").Funcs(mergeFuncMaps(funcMaps...))
	return tmpl, fs.WalkDir(embedFs, ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}
		b, err := embedFs.ReadFile(path)
		if err != nil {
			return err
		}
		if _, err = tmpl.New(path).Parse(string(b)); err != nil {
			return fmt.Errorf("Unable to parse: path=%s err=%v", path, err)
		}
		return nil
	})
}
