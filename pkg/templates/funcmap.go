package templates

import (
	"fmt"
	"html/template"
	"regexp"
	"strings"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

var reCSSID = regexp.MustCompile(`[^a-zA-Z0-9]*`)

// FuncMap for generic use
var FuncMap = template.FuncMap{
	"CSSID": func(s string) string {
		return reCSSID.ReplaceAllString(s, "")
	},
	"Join": func(s []string) string {
		return strings.Join(s, ", ")
	},
	"TrimSpace": strings.TrimSpace,
	"IntWithCommas": func(f int) string {
		p := message.NewPrinter(language.English)
		return p.Sprintf("%d", f)
	},
	"Float32WithCommas": func(f float32, d ...int) string {
		decimals := 2
		if len(d) > 0 {
			decimals = d[0]
		}
		p := message.NewPrinter(language.English)
		return p.Sprintf(fmt.Sprintf("%%.%df", decimals), f)
	},
}
