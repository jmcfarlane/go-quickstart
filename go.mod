module gitlab.com/jmcfarlane/go-quickstart

go 1.16

require (
	github.com/jmcfarlane/httprwd v0.0.0-20161101214659-0d95a7509b53
	github.com/prometheus/client_golang v1.11.0
	github.com/rs/zerolog v1.23.0
	golang.org/x/text v0.3.3
)
